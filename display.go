package main

import (
	"github.com/gdamore/tcell/v2"
	"github.com/msoap/tcg"
	"github.com/rs/zerolog"
)

type Display struct {
	tcg       *tcg.Tcg
	eventChan chan Event

	logger zerolog.Logger
}

type Event string

func NewDisplay(l zerolog.Logger) (*Display, chan Event) {
	tg, err := tcg.New(tcg.Mode2x3) // each terminal symbol contains a 2x3 pixels grid, also you can use 1x1, 1x2, and 2x2 modes
	if err != nil {
		panic("failed to create display")
	}

	eventChan := make(chan Event)

	d := &Display{
		tcg:       tg,
		eventChan: eventChan,

		logger: l.With().Caller().Logger(),
	}

	go d.startPollEvents()

	// Present a new clear screen
	d.Clear()

	return d, eventChan
}

func (d *Display) startPollEvents() {
	for {
		event := d.tcg.TCellScreen.PollEvent()
		switch event := event.(type) {
		case *tcell.EventKey:
			d.eventChan <- Event(event.Rune())
		}
	}
}

// sync updates the screen to match buffer
func (d *Display) sync() {
	d.tcg.Show()
}

func (d *Display) Destroy() {
	d.tcg.Finish()
}

// Clear clears the display
func (d *Display) Clear() {
	d.tcg.Buf.Clear()
	d.sync()
}
