package main

import "github.com/rs/zerolog"

const InitialPCHex = Hex("0200")

type display interface {
	Clear()
	Destroy()
}

type Chip8 struct {
	mem     *Memory
	cpu     *CPU
	display display

	logger zerolog.Logger
}

type Chip8Opt func(*Chip8) *Chip8

func NewChip8(
	mem *Memory,
	display display,
	logger zerolog.Logger,
) Chip8 {
	w, err := HexToWord(InitialPCHex)
	if err != nil {
		panic("InitialPCHex couldn't be converted to []byte")
	}

	mem.SetPC(w)

	cpu := NewCPU(
		mem,
		display,
		logger,
	)

	return Chip8{
		mem:     mem,
		cpu:     cpu,
		display: display,

		logger: logger.With().Caller().Logger(),
	}
}

// Start begins the fetch, decode and execute loop
func (c *Chip8) Start() {
	c.cpu.Start()
}

func (c *Chip8) Stop() {
	c.cpu.Stop()
}
