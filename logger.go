package main

import (
	"io"

	"github.com/rs/zerolog"
)

func NewLogger(w io.Writer) zerolog.Logger {
	return zerolog.
		New(zerolog.ConsoleWriter{
			Out: w,
		}).
		Level(zerolog.DebugLevel).
		With().Timestamp().
		Logger()
}
