package main

import (
	"fmt"
	"testing"

	"github.com/rs/zerolog"
)

func TestDecodeJump(t *testing.T) {
	mem := NewMemory()

	jumpI, err := HexToBytes("15A1")
	if err != nil {
		t.Errorf("Failed to encode jump instruction to bytes: %v", err)
	}
	mainMem := [4096]byte{}
	copy(mainMem[:], jumpI)
	mem.main = mainMem

	cpu := NewCPU(mem, MockDisplay{}, zerolog.Nop())
	cpu.Decode(mem.FetchFromPC())

	expectedBytes, err := HexToWord("05a1")
	if err != nil {
		t.Errorf("Failed to convert expectedBytes: %v", err)
	}

	if mem.pc != expectedBytes {
		t.Errorf("CurrentPC isn't correct: Expected %v, Got %v", "05a1", WordToHex(mem.pc))
	}
}

func TestDecodeSet(t *testing.T) {
	mem := NewMemory()

	// Set variable register 1 to AA
	setI, err := HexToBytes("61AA")
	if err != nil {
		t.Errorf("Failed to encode set instruction to bytes: %v", err)
	}
	mainMem := [4096]byte{}
	copy(mainMem[:], setI)
	mem.main = mainMem

	cpu := NewCPU(mem, MockDisplay{}, zerolog.Nop())
	cpu.Decode(mem.FetchFromPC())

	expectedBytes, err := HexToBytes("AA")
	if err != nil {
		t.Errorf("Failed to convert expectedBytes: %v", err)
	}

	if mem.v[1] != expectedBytes[0] {
		t.Errorf("CurrentPC isn't correct: Expected %v, Got %v", expectedBytes, mem.v[1])
	}
}

func TestDecodeAdd(t *testing.T) {
	mem := NewMemory()
	variableRegisterID := 1

	// Set variable register 1 to FF
	addI, err := HexToBytes(Hex(fmt.Sprintf("7%vFE", variableRegisterID)))
	if err != nil {
		t.Errorf("Failed to encode add instruction to bytes: %v", err)
	}
	mainMem := [4096]byte{}
	copy(mainMem[:], addI)
	mem.main = mainMem

	mem.v[1] = 1

	cpu := NewCPU(mem, MockDisplay{}, zerolog.Nop())
	cpu.Decode(mem.FetchFromPC())

	expectedBytes, err := HexToBytes("FF")
	if err != nil {
		t.Errorf("Failed to convert expectedBytes: %v", err)
	}

	if mem.v[1] != expectedBytes[0] {
		t.Errorf("Variable index %v isn't correct: Expected %v, Got %v", variableRegisterID, expectedBytes[0], mem.v[1])
	}
}

func TestDecodeSetIndex(t *testing.T) {
	mem := NewMemory()

	// Set variable register 1 to FF
	setIndexI, err := HexToBytes("A5A1")
	if err != nil {
		t.Errorf("Failed to encode set index instruction to bytes: %v", err)
	}
	mainMem := [4096]byte{}
	copy(mainMem[:], setIndexI)
	mem.main = mainMem

	cpu := NewCPU(mem, MockDisplay{}, zerolog.Nop())
	cpu.Decode(mem.FetchFromPC())

	expectedBytes, err := HexToWord("05A1")
	if err != nil {
		t.Errorf("Failed to convert expectedBytes: %v", err)
	}

	if mem.i != expectedBytes {
		t.Errorf("Index value isn't correct: Expected %v, Got %v", expectedBytes, mem.i)
	}
}
