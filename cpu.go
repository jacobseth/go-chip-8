package main

import (
	"strings"
	"time"

	"github.com/rs/zerolog"
)

const (
	NibbleBitmask = byte(0b11110000)
)

type CPU struct {
	mem     *Memory
	display display
	ticker  *time.Ticker
	done    chan bool

	logger zerolog.Logger
}

func NewCPU(
	mem *Memory,
	display display,
	logger zerolog.Logger,
) *CPU {
	return &CPU{
		mem:     mem,
		display: display,
		ticker:  time.NewTicker(time.Second),
		done:    make(chan bool),
		logger:  logger.With().Caller().Logger(),
	}
}

// Start begins the fetch, decode, execute loop
func (c CPU) Start() {
	for {
		select {
		case <-c.ticker.C:
			i := c.mem.FetchFromPC()
			c.Decode(i)
		case <-c.done:
			return
		}
	}
}

func (c *CPU) Stop() {
	c.done <- true
	c.ticker.Stop()
}

// Helpful Byte Printer
// fmt.Printf("%08b\n", byte(i[0])<<4|byte(i[1]>>4))

// Decode returns an Executor which will carry out the
// instruction specified by i
func (c *CPU) Decode(i Word) {
	iHex := WordToHex(i)
	nibbleHex := BytesToHex([]byte{byte(NibbleBitmask & i[0])})

	switch strings.ToUpper(string(nibbleHex[0])) {
	case `0`:
		switch iHex {
		case "00E0":
			c.logger.Debug().Msg("Clear Screen")
		case "00EE":
			c.logger.Debug().Msg("Subroutine")
		default:
			c.logger.Debug().Msg("Execute machine language routine")
			// Assume 0NNN: Execute machine language routine; do nothing
		}
	case `1`:
		// 1NNN
		c.logger.Debug().Msg("Jump")

		jmpAddr := NNN(i)
		c.mem.SetPC(jmpAddr)
	case `6`:
		c.logger.Debug().Msg("Set")

		register := X(i)
		c.mem.v[register] = NN(i)
	case `7`:
		c.logger.Debug().Msg("Add")

		x := X(i)
		vx := c.mem.v[x]
		newVal := vx + NN(i)
		c.mem.v[x] = newVal
	case `A`:
		c.logger.Debug().Msg("Set Index")

		c.mem.i = NNN(i)
	}
}

// NNN Extracts second, third and fourth nibbles from a provided word
func NNN(i Word) Word {
	return Word([2]byte{byte(i[0]) & ^NibbleBitmask, byte(i[1])})
}

func NN(i Word) byte {
	return i[1]
}

func X(i Word) byte {
	return i[0] & ^NibbleBitmask
}
