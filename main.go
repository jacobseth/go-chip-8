package main

import (
	"os"
)

func main() {
	f, err := os.OpenFile("log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	logger := NewLogger(f)

	logger.
		Debug().
		Msg("Chip-8 Starting up")

	d, eventChan := NewDisplay(
		logger,
	)

	chip8 := NewChip8(
		NewMemory(),
		d,
		logger,
	)

	go chip8.Start()
	for event := range eventChan {
		if event == Event("q") {
			break
		}
	}

	logger.Debug().Msg("Chip-8 Shutting down")
	chip8.Stop()
	d.Destroy()
}
