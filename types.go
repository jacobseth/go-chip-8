package main

import (
	"encoding/binary"
	hexEnc "encoding/hex"
	"strconv"
)

type Hex string

func HexToWord(h Hex) (Word, error) {
	b, err := hexEnc.DecodeString(string(h))
	if err != nil {
		return Word{}, err
	}

	w := Word{}
	copy(w[:], b)

	return w, nil
}

func WordToHex(w Word) Hex {
	return Hex(hexEnc.EncodeToString(w[:]))
}

func HexToBytes(h Hex) ([]byte, error) {
	return hexEnc.DecodeString(string(h))
}

func BytesToHex(b []byte) Hex {
	return Hex(hexEnc.EncodeToString(b))
}

func HexToInt(h Hex) (int64, error) {
	return strconv.ParseInt(string(h), 16, 64)
}

func WordToInt(w Word) uint16 {
	return binary.BigEndian.Uint16(w[:])
}

func IntToWord(i uint16) Word {
	b := make([]byte, 16)
	binary.BigEndian.PutUint16(b, i)
	w := Word{}
	copy(w[:], b)
	return w
}
