package main

const (
	MainMemorySize       = 4096
	VariableRegisterSize = 16
)

type Word [2]byte
type Stack []Word

type Memory struct {
	main  [MainMemorySize]byte
	i     Word
	pc    Word
	stack Stack
	v     [VariableRegisterSize]byte
}

func NewMemory() *Memory {
	return &Memory{
		main:  [MainMemorySize]byte{},
		i:     Word{},
		pc:    Word{},
		stack: Stack{},
		v:     [VariableRegisterSize]byte{},
	}
}

func (m *Memory) SetPC(addr Word) {
	m.pc = addr
}

// FetchFromPC returns the instruction Word (2 bytes) that PC is
// pointing to, then increments PC by 2 so it's ready to
// read the next instruction.
func (m *Memory) FetchFromPC() Word {
	defer m.SetPC(IntToWord(WordToInt(m.pc) + 2))
	return Word{m.main[WordToInt(m.pc)], m.main[WordToInt(m.pc)+1]}
}
