package main

import (
	"testing"

	"github.com/rs/zerolog"
)

type MockDisplay struct{}

func (_ MockDisplay) Clear()   {}
func (_ MockDisplay) Destroy() {}

// TestNewCPU calls NewCPU and checks that
// it is initialized correctly.
func TestNewChip8(t *testing.T) {
	m := NewMemory()
	_ = NewChip8(
		m,
		MockDisplay{},
		zerolog.Nop(),
	)

	if m.pc != [2]byte{2, 0} {
		t.FailNow()
	}
}
